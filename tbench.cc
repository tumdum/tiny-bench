#include "tbench.h"
#include <iostream>
#include <memory>
#include <vector>
#include <stdint.h>
#include <ctime>
#include <limits>
#include <sstream>

namespace tinybench
{

namespace detail
{

void (*useIt)(void*) = 0;
void realUseIt(void*) {}

Benchmarks& benchmarks__()
{
    static Benchmarks benchmarks;
    return benchmarks;
}

}  // namespace detail

namespace
{

uint64_t runBenchmark(std::auto_ptr<BenchmarkBase> solution);

uint64_t monotonicTimeNow()
{
    static const uint64_t kSecInNsec = 1000000000;
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts.tv_sec * kSecInNsec + ts.tv_nsec;
}

uint64_t measureMinimalTimeDiff()
{
    static const int kMeasurements = 10000;
    uint64_t min = std::numeric_limits<uint64_t>::max();
    for (int i = 0; i < kMeasurements; ++i)
    {
        uint64_t start = monotonicTimeNow();
        uint64_t diff = monotonicTimeNow() - start;
        if (diff < min)
            min = diff;
    }
    return min;
}

size_t findMinimalSize(const detail::SolutionBuilder& builder,
    const uint64_t requiredSampleCount)
{
    static const uint64_t minimalTimeDiff = measureMinimalTimeDiff();
    uint64_t time = 0;
    uint64_t size = 1;
    const uint64_t minTime = requiredSampleCount * minimalTimeDiff;
    do
    {
        size *= 2;
        time = runBenchmark(std::auto_ptr<BenchmarkBase>(builder.build(size)));
    } while (time <= minTime);
    return size-1;
}

size_t findGoodSize(const detail::Solutions& solutions)
{
    static const uint64_t kMinSampleCount = 1000;
    size_t goodTime = 0;
    for (detail::Solutions::const_iterator i = solutions.begin();
        i != solutions.end(); ++i)
    {
        const size_t t = findMinimalSize(*i, kMinSampleCount);
        if (t > goodTime) goodTime = t;
    }
    return goodTime;
}

uint64_t runBenchmark(std::auto_ptr<BenchmarkBase> solution)
{
    const size_t kSolutionExecutions = 100;
    uint64_t best = solution->run();
    for (size_t i = 0; i < kSolutionExecutions; ++i)
    {
        uint64_t current = solution->run();
        if (best > current) best = current;
    }
    return best;
}

size_t findMaxNameLenght(const detail::Solutions& solutions)
{
    size_t l = 0;
    for (detail::Solutions::const_iterator i = solutions.begin();
        i != solutions.end(); ++i)
    {
        if (l < i->name.size())
            l = i->name.size();
    }
    return l;
}

std::string indent(const std::string& name, const size_t maxLength)
{
    const size_t diff = maxLength - name.size();
    std::string prefix(diff, ' ');
    return prefix + name;
}

void runAllSolutions(const detail::Solutions& solutions, const size_t s)
{
    const size_t maxNameLenght = findMaxNameLenght(solutions);
    for (detail::Solutions::const_iterator i = solutions.begin();
        i != solutions.end(); ++i)
    {
        std::auto_ptr<BenchmarkBase> benchmark(i->build(s));
        const uint64_t t = runBenchmark(benchmark);
        std::cerr << "\t" << indent(i->name, maxNameLenght) << ": " << t << std::endl;
    }
}

}  // namespace

int InitAndRunBenchmarks(int , char** )
{
    detail::useIt = &detail::realUseIt;
    for (detail::Benchmarks::const_iterator i = detail::benchmarks__().begin();
        i != detail::benchmarks__().end(); ++i)
    {
        std::cerr << i->first << " has " << i->second.size()
            << " solutions:" << std::endl;
        const size_t size = findGoodSize(i->second);
        runAllSolutions(i->second, size);
    }
    return 0;
}

uint64_t BenchmarkBase::run()
{
    start();
    solution();
    stop();
    return t_;
}

void BenchmarkBase::start()
{
    t_ = monotonicTimeNow();
}

void BenchmarkBase::stop()
{
    t_ = monotonicTimeNow() - t_;
}

}  // namespace tinybench
