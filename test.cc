#include "tbench.h"
#include <sstream>
#include <cstdlib>
#include <iostream>
#include <boost/functional/hash.hpp>

size_t my_hash(const std::string& s)
{
    size_t h = s.size();
    for (size_t i = 0; i != s.size(); ++i)
        h *= s[i];
    return h;
}

size_t silly_hash(const std::string& s)
{
    size_t h = 0;
    for (size_t i = 0; i != s.size(); ++i)
    {
        for (size_t j = 0; j != s.size() / 4; ++j)
        {
            h += s[i] * s[j];
        }
    }
    return h;
}

std::string StringHashingSetup(const size_t size)
{
    static const char alphabet[] = "abcdefghijklmnopqrstuvwxyz";
    const size_t length = sizeof(alphabet) / sizeof(alphabet[0]);
    std::stringstream s;
    size_t left = size;
    while (left--) { s << alphabet[random() % length]; }
    return s.str();
}

SETUP(StringHashing, std::string)
{
    static const char alphabet[] = "abcdefghijklmnopqrstuvwxyz";
    const size_t length = sizeof(alphabet) / sizeof(alphabet[0]);
    std::stringstream s;
    while (size--) { s << alphabet[random() % length]; }
    return s.str();
}

BENCHMARK(StringHashing, BoostHash)
{
    FORCE(boost::hash<std::string>()(GetParam()));
}

BENCHMARK(StringHashing, OwnHash)
{
    FORCE(my_hash(GetParam()));
}

BENCHMARK(StringHashing, SillyHash)
{
    FORCE(silly_hash(GetParam()));
}

size_t fib(size_t n)
{
    if (n == 0 || n == 1) return 1;
    return fib(n-1) + fib(n-2);
}

size_t fib_tail_aux(size_t n, size_t curr, size_t next)
{
    if (n == 0) return curr;
    return fib_tail_aux(n-1, next, curr + next);
}
size_t fib_tail(size_t n) { return fib_tail_aux(n, 0, 1); }

size_t fib_loop(size_t n) {
    if (n == 0 || n == 1)
        return n;
    size_t curr = 0, next = 1;
    while (n) {
        size_t tmp = curr;
        curr = next;
        next = tmp + curr;
        --n;
    }
    return curr;
}

SETUP(FibonacciNumber, size_t)
{
    return size;
}

BENCHMARK(FibonacciNumber, TailCallRecursive)
{
    FORCE(fib_tail(GetParam()));
}

BENCHMARK(FibonacciNumber, Loop)
{
    FORCE(fib_loop(GetParam()));
}

int main(int argc, char** argv)
{
    return RUN_ALL_BENCHMARKS(argc, argv);
}
