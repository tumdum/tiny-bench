#ifndef TBENCH_H_
#define TBENCH_H_

#include <map>
#include <vector>
#include <string>
#include <stdint.h>

namespace tinybench
{

struct BenchmarkBase
{
    virtual ~BenchmarkBase() {}
    uint64_t run();
private:
    void start();
    void stop();
    virtual void solution() = 0;
    size_t t_;
};

template <typename T>
struct Benchmark : BenchmarkBase
{
    Benchmark(const T param) : param_(param) {}
    const T& GetParam() const { return param_; }
private:
    const T param_;
};

int InitAndRunBenchmarks(int argc, char** argv);

namespace detail
{

typedef BenchmarkBase* (*SolutionCtor) (const size_t);
struct SolutionBuilder
{
    SolutionBuilder(const std::string& name, SolutionCtor ctor)
        : name(name), ctor(ctor)
    {
    }

    BenchmarkBase* build(const size_t size) const
    {
        return ctor(size);
    }

    std::string name;
private:
    SolutionCtor ctor;
};
typedef std::vector<SolutionBuilder> Solutions;
typedef std::map<std::string, Solutions> Benchmarks;

extern Benchmarks& benchmarks__();

template <typename T>
void* toVoidPtr(const T& t)
{
    return reinterpret_cast<void*>(const_cast<T*>(&t));
}

extern void (*useIt)(void*);

}  // namespace detail

}  // namespace tinybench

#define SETUP(ProblemName, InputType)                                         \
    struct ProblemName##BENCHMARK__ : tinybench::Benchmark<InputType>         \
    {                                                                         \
        static InputType prepareData(size_t);                                 \
        ProblemName##BENCHMARK__(const size_t size)                           \
            : tinybench::Benchmark<InputType>(prepareData(size))              \
        {                                                                     \
        }                                                                     \
    };                                                                        \
    InputType ProblemName##BENCHMARK__::prepareData(size_t size)


#define BENCHMARK(ProblemName, ProblemSolution)                               \
    struct ProblemName##ProblemSolution##BENCHMARK__                          \
        : ProblemName##BENCHMARK__                                            \
    {                                                                         \
        ProblemName##ProblemSolution##BENCHMARK__(const size_t s)             \
            : ProblemName##BENCHMARK__(s) {}                                  \
        void solution();                                                      \
    private:                                                                  \
        static void* dummy__;                                                 \
        static void* registerBenchmark__();                                   \
    };                                                                        \
    tinybench::BenchmarkBase*                                                 \
        makeProblemName##ProblemSolution##BENCHMARK__(const size_t s)         \
    {                                                                         \
        return new ProblemName##ProblemSolution##BENCHMARK__(s);              \
    }                                                                         \
    void* ProblemName##ProblemSolution##BENCHMARK__::registerBenchmark__()    \
    {                                                                         \
        tinybench::detail::SolutionBuilder builder                            \
            (#ProblemSolution,                                                \
            &makeProblemName##ProblemSolution##BENCHMARK__);                  \
        tinybench::detail::benchmarks__()[#ProblemName]                       \
            .push_back(builder);                                              \
        return static_cast<void*>(0);                                         \
    }                                                                         \
    void* ProblemName##ProblemSolution##BENCHMARK__::dummy__ =                \
        ProblemName##ProblemSolution##BENCHMARK__::registerBenchmark__();     \
    void ProblemName##ProblemSolution##BENCHMARK__::solution()

#define RUN_ALL_BENCHMARKS(ARGC, ARGV)                                        \
    tinybench::InitAndRunBenchmarks((ARGC), (ARGV));

#define FORCE(S) tinybench::detail::useIt(tinybench::detail::toVoidPtr(S));

#endif  // TBENCH_H_
