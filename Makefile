CC=g++
CXXFLAGS=-Wall -Wextra -pedantic -std=c++98 -O2

test: test.cc tbench.cc tbench.h
	$(CC) $(CXXFLAGS) tbench.cc test.cc -o test
	./test

clean:
	rm -f test
